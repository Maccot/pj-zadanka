//#pakage author.writer;

import java.util.concurrent.locks.*;


public class Teksty {

    Lock lock = new ReentrantLock();
    Condition txtWritten = lock.newCondition();
    Condition txtSupplied = lock.newCondition();

    String txt = null;
    boolean newTxt = false;

    //Metoda ustalająca tekst -- wywołuje Autor

    public void setTextToWrite(String s){
        lock.lock();
        try {
            if (txt != null){
                while ( (newTxt == true))
                    txtWritten.await();
            }
            txt= s;
            newTxt =true;
            txtSupplied.signal();
        }catch (InterruptedException exc){
        } finally {
            lock.unlock();
        }
    }

    //Metoda pobrania tekstu - wywołuje writer
    public String getTextToWrite(){
        lock.lock();
        try {
            while (newTxt == false)
                txtSupplied.await(); //moze byc interupted
            newTxt = false;
            txtWritten.signal();
            return txt;
        }catch (InterruptedException exc){
            return null;
        }finally {
            lock.unlock();
        }
    }
}

